import { useState, useEffect } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import { FaPlus } from 'react-icons/fa';
import styles from '../../styles/Transactions.module.css';
import View from '../../components/View';
import AppHelper from '../../apphelper';
import DisplayTransaction from '../../components/DisplayTransactions';

const Transactions = () => {
  const [searchKeyword, setSearchKeyword] = useState('');
  const [searchType, setSearchType] = useState('All');
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    const token = AppHelper.getAccessToken();
    // Get Transactions
    fetch(`${AppHelper.API_URL}/api/users/transaction-details`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        setTransactions(data);
      });
  }, []);

  const filteredItems = transactions.filter((transaction) => {
    if (searchType === 'Income') {
      return transaction.transactionType === 'Income';
    }
    if (searchType === 'Expense') {
      return transaction.transactionType === 'Expense';
    }
    return transaction;
  });

  const searchedItems = filteredItems.filter((item) => {
    return item.description
      .toLowerCase()
      .includes(searchKeyword.toLocaleLowerCase());
  });

  return (
    <>
      <View title="Spendwise | Transactions" />
      <main className="container">
        <div className={styles.formContainer}>
          <h3 className="form-heading">Transactions</h3>
          <form className={styles.form}>
            <div className={styles.btnRow}>
              <Link href="/user/add-transactions">
                <a className={styles.formBtnWithIcon}>
                  <FaPlus />
                  <span>Add Transactions</span>
                </a>
              </Link>
              <Link href="/user/add-category">
                <a className={styles.formBtnWithIcon}>
                  <FaPlus />
                  <span>Add Category</span>
                </a>
              </Link>
            </div>
            <label className={styles.formLabel}>Search Transaction</label>
            <div className={styles.formRow}>
              <input
                className={styles.formInputFlex1}
                type="text"
                placeholder="Search Record"
                value={searchKeyword}
                onChange={(e) => setSearchKeyword(e.target.value)}
              />
              <select
                className={styles.formSelect}
                defaultValue={searchType}
                onChange={(e) => setSearchType(e.target.value)}
              >
                <option value="All">All</option>
                <option value="Income">Income</option>
                <option value="Expense">Expense</option>
              </select>
            </div>
          </form>
          <div className={styles.transactions}>
            <h4>Your Transactions</h4>
            <DisplayTransaction
              transactions={searchedItems}
              searchKeyword={searchKeyword}
              searchType={searchType}
            />
          </div>
        </div>
      </main>
    </>
  );
};

export default Transactions;

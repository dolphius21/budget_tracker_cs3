import { useState, useEffect } from 'react';
import { InputGroup, Form, Col } from 'react-bootstrap';
import moment from 'moment';
import randomcolor from 'randomcolor';
import AppHelper from '../../apphelper';
import { Pie } from 'react-chartjs-2';
import View from '../../components/View';

const categoryBreakdown = () => {
  const [fromDate, setFromDate] = useState(
    moment().subtract(1, 'months').format('YYYY-MM-DD')
  );
  const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'));
  const [labelsArr, setLabelsArr] = useState([]);
  const [dataArr, setDataArr] = useState([]);
  const [bgColors, setBgColors] = useState([]);

  const data = {
    labels: labelsArr,
    datasets: [
      {
        data: dataArr,
        backgroundColor: bgColors
      }
    ]
  };

  useEffect(() => {
    const payload = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      },
      body: JSON.stringify({
        fromDate: fromDate,
        toDate: toDate
      })
    };

    fetch(`${AppHelper.API_URL}/api/users/get-transaction-summary`, payload)
      .then((res) => res.json())
      .then((records) => {
        setLabelsArr(records.map((record) => record.categoryName));
        setDataArr(records.map((record) => record.totalAmount));
        setBgColors(records.map(() => randomcolor()));
      });
  }, [fromDate, toDate]);

  return (
    <>
      <View title="Category Breakdown" />
      <main className="container">
        <h3 className="form-heading">Category Breakdown</h3>
        <Form.Row>
          <Form.Group as={Col} xs="6">
            <Form.Label>From</Form.Label>
            <Form.Control
              type="date"
              value={fromDate}
              onChange={(e) => setFromDate(e.target.value)}
            />
          </Form.Group>
          <Form.Group as={Col} xs="6">
            <Form.Label>To</Form.Label>
            <Form.Control
              type="date"
              value={toDate}
              onChange={(e) => setToDate(e.target.value)}
            />
          </Form.Group>
        </Form.Row>
        <hr />
        <Pie data={data} />
      </main>
    </>
  );
};

export default categoryBreakdown;

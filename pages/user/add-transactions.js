import { useState, useEffect } from 'react';
import Link from 'next/link';
import View from '../../components/View';
import AppHelper from '../../apphelper';
import Swal from 'sweetalert2';

const AddTransactions = () => {
  const [categories, setCategories] = useState([]);
  const [categoryName, setCategoryName] = useState('');
  const [typeName, setTypeName] = useState('');
  const [amount, setAmount] = useState(0);
  const [description, setDescription] = useState('');

  useEffect(() => {
    const token = AppHelper.getAccessToken();

    fetch(`${AppHelper.API_URL}/api/users/get-categories`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
        'Acccess-Control-Allow-Origin': '*'
      }
    })
      .then((res) => res.json())
      .then((data) => setCategories(data));

    return () => {
      AppHelper.getAccessToken();
    };
  }, []);

  const addTransaction = (e) => {
    e.preventDefault();
    console.log(categoryName);

    fetch(`${AppHelper.API_URL}/api/users/add-transaction`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      },
      body: JSON.stringify({
        categoryName: categoryName,
        type: typeName,
        amount: amount,
        description: description
      })
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Transaction Added'
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Fail to add transaction',
            text: 'Something went wrong, check your inputs'
          });
        }
      });
  };

  return (
    <>
      <View title="Spendwise | Add Transaction" />
      <main className="form-container">
        <h4 className="form-heading">Add Transactions</h4>
        <form className="form" onSubmit={(e) => addTransaction(e)}>
          <div className="form-group">
            <label>Transaction Type</label>
            <select
              value={typeName}
              onChange={(e) => setTypeName(e.target.value)}
              required
            >
              <option value selected>
                Select Transaction Type
              </option>
              <option value="Income">Income</option>
              <option value="Expense">Expense</option>
            </select>
          </div>
          <div className="form-group">
            <label>Category:</label>
            <select
              value={categoryName}
              onChange={(e) => setCategoryName(e.target.value)}
              required
            >
              <option value selected>
                Select Category
              </option>
              {categories.map((category) => {
                return (
                  <option key={category._id} value={category.name}>
                    {category.name}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="form-group">
            <label>Amount:</label>
            <input
              type="number"
              placeholder="Enter amount"
              value={amount}
              onChange={(e) => setAmount(parseFloat(e.target.value))}
              required
            />
          </div>
          <div className="form-group">
            <label>Description:</label>
            <textarea
              type="text"
              rows="3"
              placeholder="Enter description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              required
            ></textarea>
          </div>
          <button className="form-btn" type="submit">
            Add
          </button>
          <p className="form-link">
            Want to see your transactions?
            <Link href="/user/transactions">
              <a> Go to Transactions</a>
            </Link>
          </p>
        </form>
      </main>
    </>
  );
};

export default AddTransactions;

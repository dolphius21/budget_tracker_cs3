import { useState } from 'react';
import Link from 'next/link';
import View from '../../components/View';
import AppHelper from '../../apphelper';
import Swal from 'sweetalert2';

const Categories = () => {
  const [categoryName, setCategoryName] = useState('');
  const [typeName, setTypeName] = useState(undefined);

  const addCategory = (e) => {
    e.preventDefault();
    const token = AppHelper.getAccessToken();
    fetch(`${AppHelper.API_URL}/api/users/add-category`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: categoryName,
        type: typeName
      })
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Category Added'
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Fail to add category',
            text: 'Something went wrong, check your inputs'
          });
        }
      });
  };

  return (
    <>
      <View title="Spendwise | Add Category" />
      <main className="form-container">
        <h3 className="form-heading">Add Category</h3>
        <form className="form" onSubmit={(e) => addCategory(e)}>
          <div className="form-group">
            <label>Category Name:</label>
            <input
              type="text"
              placeholder="Enter category name"
              value={categoryName}
              onChange={(e) => setCategoryName(e.target.value)}
              required
            />
          </div>
          <div className="form-group">
            <label>Type</label>
            <select
              value={typeName}
              onChange={(e) => setTypeName(e.target.value)}
              required
            >
              <option value selected disabled>
                Select Transaction Type
              </option>
              <option value="Income">Income</option>
              <option value="Expense">Expense</option>
            </select>
          </div>
          <button className="form-btn" type="submit">
            Add
          </button>
          <p className="form-link">
            Want to see your transactions?
            <Link href="/user/transactions">
              <a> Go to Transactions</a>
            </Link>
          </p>
        </form>
      </main>
    </>
  );
};

export default Categories;

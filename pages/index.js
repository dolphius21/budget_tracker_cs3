import Banner from '../components/Banner';
import NavBar from '../components/NavBar';
import View from '../components/View';

export default function Home() {
  return (
    <div>
      <View title={'Spendwise'} />
      <Banner />
    </div>
  );
}

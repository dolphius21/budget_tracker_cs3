import { useEffect, useContext } from 'react';
import Router from 'next/router';
import View from '../../components/View';
import UserContext from '../../contexts/UserContext';

const Logout = () => {
  const { unsetUser } = useContext(UserContext);

  useEffect(() => {
    unsetUser();
    Router.push('/login');
  }, []);
  return (
    <>
      <View title="Spendwise | Logout" />
      <main className="container">
        <h5 className="text-center">Logging out...</h5>
        <h6 className="text-center">
          You will be redirected back to the login page shortly.
        </h6>
      </main>
    </>
  );
};

export default Logout;

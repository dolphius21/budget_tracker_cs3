import { useState } from 'react';
import GoogleBtn from '../components/GoogleBtn';
import AppHelper from '../apphelper';
import Swal from 'sweetalert2';
import Router from 'next/router';
import View from '../components/View';

const Register = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [vPassword, setVPassword] = useState('');

  const isDisabled =
    !firstName || !lastName || !mobileNo || !email || password !== vPassword;

  const registerUser = (e) => {
    e.preventDefault();

    fetch(`${AppHelper.API_URL}/api/users/email-exists`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email
      })
    })
      .then((res) => res.json())
      .then((data) => {
        if (!data) {
          fetch(`${AppHelper.API_URL}/api/users/register`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
              firstName,
              lastName,
              email,
              mobileNo,
              password
            })
          })
            .then((res) => res.json())
            .then((data) => {
              if (data) {
                Swal.fire({
                  icon: 'success',
                  title: 'Successfully Registered',
                  text: 'Thank you for registering.'
                });
                Router.push('/login');
              } else {
                Swal.fire({
                  icon: 'error',
                  title: 'Registration Failed',
                  text: 'Something went wrong!'
                });
              }
            })
            .catch((err) => console.log(err));
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Registration Failed',
            text: 'Email already exists! Try another email.'
          });
        }
      });
  };

  return (
    <>
      <View title="Spendwise | Sign Up" />
      <main className="form-container">
        <h1 className="form-heading">Create Account</h1>

        <form className="form" onSubmit={(e) => registerUser(e)}>
          <div className="form-group">
            <label>First Name</label>
            <input
              type="text"
              placeholder="Enter your first name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label>Last Name</label>
            <input
              type="text"
              placeholder="Enter your last name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label>Mobile No.</label>
            <input
              type="text"
              placeholder="Enter your mobile no."
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label>Email Address</label>
            <input
              type="email"
              placeholder="Enter your email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              placeholder="Enter your password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label>Confirm Password</label>
            <input
              type="password"
              placeholder="Re-enter your password"
              value={vPassword}
              onChange={(e) => setVPassword(e.target.value)}
            />
          </div>
          <button className="form-btn" disabled={isDisabled}>
            Submit
          </button>
          <div className="google-text">
            <span className="line-1"></span>
            <p>or</p>
            <span className="line-2"></span>
          </div>
          <GoogleBtn text={'Sign Up'} />
        </form>
      </main>
    </>
  );
};

export default Register;

import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
import { UserProvider } from '../contexts/UserContext';
import AppHelper from '../apphelper';
import AppNavBar from '../components/NavBar';

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({ email: null });

  useEffect(() => {
    if (AppHelper.getAccessToken) {
      const payload = {
        headers: { Authorization: `Bearer ${AppHelper.getAccessToken()}` }
      };
      fetch(`${AppHelper.API_URL}/api/users/details`, payload)
        .then((res) => res.json())
        .then((userData) => {
          if (userData.email) {
            setUser({ email: userData.email });
          } else {
            setUser({ email: null });
          }
        });
    }
  }, [user.id]);

  const unsetUser = () => {
    localStorage.clear();
    setUser({ email: null });
  };

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <AppNavBar />
        <Component {...pageProps} />
      </UserProvider>
    </>
  );
}

export default MyApp;

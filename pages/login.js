import Router from 'next/router';
import { useState, useContext } from 'react';
import Link from 'next/link';
import AppHelper from '../apphelper';
import Swal from 'sweetalert2';
import GoogleBtn from '../components/GoogleBtn';
import UserContext from '../contexts/UserContext';
import View from '../components/View';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { user, setUser } = useContext(UserContext);

  const isDisabled = !email || !password;

  const retrieveUserDetails = (accessToken) => {
    const payload = {
      headers: { Authorization: `Bearer ${accessToken}` }
    };
    fetch(`${AppHelper.API_URL}/api/users/details`, payload)
      .then((res) => res.json())
      .then((data) => {
        setUser({ email: data.email });
        Router.push('/user/transactions');
      });
  };

  const loginUser = (e) => {
    e.preventDefault();

    fetch(`${AppHelper.API_URL}/api/users/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email,
        password
      })
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.accessToken) {
          localStorage.setItem('token', data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            icon: 'success',
            title: 'Login Successfully'
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Login Unsuccessfully',
            text: 'Something went wrong, check your credentials'
          });
        }
      });
  };

  return (
    <>
      <View title="Spendwise | Sign In" />
      <div className="container">
        <main className="form-container">
          <h1 className="form-heading">Welcome Back!</h1>
          <form className="form" onSubmit={(e) => loginUser(e)}>
            <div className="form-group">
              <label>Email Address</label>
              <input
                type="email"
                placeholder="Enter your email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                placeholder="Enter your password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <button className="form-btn" disabled={isDisabled}>
              Submit
            </button>
            <div className="google-text">
              <span className="line-1"></span>
              <p>or</p>
              <span className="line-2"></span>
            </div>
            <GoogleBtn text={'Sign In'} />
            <p className="form-link">
              Dont have an account?
              <Link href="/register">
                <a> Sign up</a>
              </Link>
            </p>
          </form>
        </main>
      </div>
    </>
  );
};

export default Login;

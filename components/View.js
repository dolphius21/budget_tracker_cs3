import Head from 'next/head';

const View = ({ title }) => {
  return (
    <>
      <Head>
        <title key="title-tag">{title}</title>
        <meta
          key="title-meta"
          name="viewport"
          content="initial-scale=1.0, width=device-width"
        />
      </Head>
    </>
  );
};

export default View;

import React, { useState, useEffect, useContext } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import UserContext from '../contexts/UserContext';
import { FaBars } from 'react-icons/fa';
import { FaTimes } from 'react-icons/fa';

const NavBar = () => {
  const [showLinks, setShowLinks] = useState(false);
  const { user } = useContext(UserContext);

  const handleResize = () => {
    const windowWidth = window.innerWidth;
    if (windowWidth > 800) {
      setShowLinks(false);
    }
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [setShowLinks]);

  const userIsNotLoginNav = (
    <>
      <Link href="/login">
        <a>Sign In</a>
      </Link>
      <Link href="/register">
        <a>Sign Up</a>
      </Link>
    </>
  );

  const userIsLoginNav = (
    <>
      <Link href="/user/transactions">
        <a>Transactions</a>
      </Link>
      <Link href="/user/summary">
        <a>Summary</a>
      </Link>
      <Link href="/logout">
        <a>Sign Out</a>
      </Link>
    </>
  );

  return (
    <>
      <nav>
        <div className="nav-center">
          <div className="nav-header">
            <Link href="/">
              <Image
                className="logo"
                src="/logo-white.svg"
                width={200}
                height={40}
              />
            </Link>
            <button
              className="nav-toggle"
              onClick={() => setShowLinks(!showLinks)}
            >
              <FaBars />
            </button>
          </div>
          <div className={`${showLinks && 'show-container'} links-container`}>
            <div
              className={`${showLinks && 'show-sidebar-header'} sidebar-header`}
            >
              <Link href="/">
                <Image
                  className="logo"
                  src="/logo-colored.svg"
                  width={200}
                  height={40}
                />
              </Link>
              <FaTimes
                className="close-btn"
                onClick={() => setShowLinks(!showLinks)}
              />
            </div>
            <div className="links">
              {user.email ? userIsLoginNav : userIsNotLoginNav}
            </div>
          </div>
        </div>
        <div className={`${showLinks && 'dark-overlay'}`}></div>
      </nav>
    </>
  );
};

export default NavBar;

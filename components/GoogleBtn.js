import { useContext } from 'react';
import Router from 'next/router';
import AppHelper from '../apphelper';
import UserContext from '../contexts/UserContext';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';

const GoogleBtn = ({ text }) => {
  const { user, setUser } = useContext(UserContext);
  const googleClientId =
    '108114658243-rjhj0iia15i3tg39tldlhkmho72abr92.apps.googleusercontent.com';

  const retrieveGoogleDetails = (res) => {
    const payload = {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify({ tokenId: res.tokenId })
    };

    fetch(`${AppHelper.API_URL}/api/users/verify-google-id-token`, payload)
      .then((res) => res.json())
      .then((data) => {
        if (data.access) {
          localStorage.setItem('token', data.access);
          Swal.fire({
            icon: 'success',
            title: 'Login Successfully'
          });
          setUser({ email: res.profileObj.email });
          Router.push('/user/transactions');
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Login Unsuccessfully',
            text: 'Something went wrong, check your credentials'
          });
        }
      });
  };

  return (
    <GoogleLogin
      render={(renderProps) => (
        <button
          className="google-btn"
          onClick={renderProps.onClick}
          disabled={renderProps.disabled}
        >
          <img src="/btn_google_dark.svg" />
          <span className="btn-text">{text} with Google</span>
        </button>
      )}
      clientId={googleClientId}
      onSuccess={retrieveGoogleDetails}
      onFailure={retrieveGoogleDetails}
      cookiePolicy={'single_host_origin'}
    />
  );
};

export default GoogleBtn;

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <p>Spendwise &copy; {new Date().getFullYear()}. All Rights Reserved.</p>
      </div>
    </footer>
  );
};

export default Footer;

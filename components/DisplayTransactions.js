import moment from 'moment';
import styles from '../styles/Transactions.module.css';

const DisplayTransactions = ({ transactions }) => {
  return (
    <>
      {transactions.map((transaction) => {
        const textColor =
          transaction.transactionType === 'Income'
            ? styles.incomeText
            : styles.expenseText;
        const amountSymbol =
          transaction.transactionType === 'Income' ? '+' : '-';
        const categoryStyle =
          transaction.transactionType === 'Income'
            ? styles.itemIncome
            : styles.itemExpense;
        return (
          <div className={styles.transactionItem} key={transaction.id}>
            <div className={styles.leftCol}>
              <div className={styles.itemTitle}>
                <p className={styles.itemDesc}>{transaction.description}</p>
                <span className={categoryStyle}>
                  {transaction.categoryName}
                </span>
              </div>
              <p className={styles.itemDate}>
                {moment(transaction.dateAdded).format('LLL')}
              </p>
            </div>
            <div className={styles.rightCol}>
              <div className={styles.itemAmount}>
                <p className={styles.amountLabel}>Amount</p>
                <p className={textColor}>
                  {amountSymbol + ' ' + transaction.amount.toLocaleString()}
                </p>
              </div>
              <div className={styles.itemAmount}>
                <p className={styles.amountLabel}>Balance</p>
                <p className={styles.balance}>
                  {transaction.balanceAfterTransaction}
                </p>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default DisplayTransactions;

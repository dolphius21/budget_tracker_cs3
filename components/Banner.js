import Link from 'next/link';
import Image from 'next/image';

const Banner = () => {
  return (
    <section className="banner">
      <div className="container">
        <h1 className="heading">Spend wisely for a better future</h1>
        <p>
          Say hello to a reliable budget tracker app that will help you with
          your finances.
        </p>
        <Link href="/register">
          <a className="btn-dark">sign up now</a>
        </Link>
        <Image
          src="/hero-graphic.svg"
          height={500}
          width={1000}
          className="banner-image"
        />
      </div>
    </section>
  );
};

export default Banner;
